Rails.application.routes.draw do


  devise_for :users, :controllers => { :omniauth_callbacks => "controllers/omniauth_callbacks" }


  root 'static_pages#home'

  resources :users do
	resources :reviews
        resources :diaries
  end
   
  resources :account_activations, only: [:edit]
  resources :posts,               only: [:create, :destroy]

  get "/users/search/", to: 'users#search'

  get    'info'      => 'static_pages#info'
  get    'aboutus'   => 'static_pages#aboutus'
  get    'contactus' => 'static_pages#contactus'
  get    'valuta'    => 'static_pages#valuta'


  get "*path", to: 'application#catch_404', via: :all
  post "*path", to: 'application#catch_404', via: :all



end
