module SessionsHelper

    # fa il login dell'utente specificato
    def log_in(user)
        session[:user_id] = user.id
    end

    # ||= operatore "or equals".
    # current_user mi permette di recuperare l'utente nel database
    # corrispondente alla session id, in pratica restituisce l'utente
    # attualmente connesso (se presente)
    def current_user
        if (user_id = session[:user_id]) # Se la sessione dello user id esiste
            @current_user ||= User.find_by(id: session[:user_id])
        elsif (user_id = cookies.signed[:user_id])
            user = User.find_by(id: user_id)
            if user && user.authenticated?(:remember, cookies[:remember_token])
                log_in user
                @current_user = user
            end
        end
    end

    # ritorna true se l'utente ha effettuato l'accesso, false altrimenti
    def logged_in?
        !current_user.nil?
    end

    # Restituisce true se l'utente specificato è l'utente corrente.
    def current_user?(user)
        user == current_user
    end

    # Ricorda un utente in una sessione persistente.
    def remember(user)
        user.remember
        cookies.permanent.signed[:user_id] = user.id
        cookies.permanent[:remember_token] = user.remember_token
    end



    # Dimentica una sessione permanente
    def forget(user)
        user.forget
        cookies.delete(:user_id)
        cookies.delete(:remember_token)
    end

    # Effettua il logout cancellando la sessione corrente
    def log_out
        forget(current_user);
        session.delete(:user_id)
        @current_user = nil
    end
end
