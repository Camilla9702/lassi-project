class StaticPagesController < ApplicationController

  def home

    if user_signed_in?
      if current_user.admin?
          @feed_items = current_user.feed_admin.paginate(page: params[:page])
      else
          @post = current_user.posts.build
          @feed_items = current_user.feed_users.paginate(page: params[:page])
      end
    end
  end


  def info
  end

  def aboutus
  end

  def contactus
  end
end
