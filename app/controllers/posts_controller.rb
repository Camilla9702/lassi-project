class PostsController < ApplicationController
     before_action :correct_user,   only: :destroy

     def new
     end

     def show
        @user = User.find(params[:id])
        @posts = @user.posts.paginate(page: params[:page])
     end

     def create
        @post = current_user.posts.build(post_params)

        #if @post.nil?
         # flash[:danger] = "Post non può essere vuoto"
          #redirect_to root_path
        if @post.save
          flash[:success] = "Post pubblicato"
          redirect_to user_path(current_user)
        else
          @feed_items = []
          #render 'static_pages/home'
          flash[:danger] = "Il post non può essere vuoto!"
          redirect_to root_path
          return
        end
     end

     def destroy
        @post.destroy
        flash[:success] = "Post eliminato"
        redirect_to request.referrer || root_url
     end

     private

     def post_params
       params.require(:post).permit(:content, :picture)
     end

     def correct_user
       @post = current_user.posts.find_by(id: params[:id])
       redirect_to root_url if @post.nil?
     end

end
