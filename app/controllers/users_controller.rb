class UsersController < ApplicationController

  before_action :current_user, only: [:show, :edit, :update, :destroy]


  def index
    @user = current_user
    @users = User.search(params[:search])
  end


  def show
   id = params[:id]
    if id.nil? then
      redirect_to root_path
      return
    end
    @user = User.find(id)
    @posts = @user.posts.paginate(page: params[:page])
    @diary = @user.diary
    @reviews = @user.reviews if !@user.reviews.empty?
  end


  def new
    @user = User.new
  end


  def edit
    @user = User.find(params[:id])
  end


  def create
    @user = User.new(user_params)
    if @user.save
        redirect_to user_path(current_user)
        #@user.send_activation_email
        #flash[:info] = "Per favore verifica la tua email per attivare il tuo account"
        #redirect_to root_url
      else
        flash[:danger] = "Utente non salvato"
        render :new
    end
  end


  def update
    @user = current_user
      if @user.update_attributes(params.require(:user).permit(:name, :surname, :email, :city, :province, :street, :birth, :desease, :picture))
        redirect_to user_path(@user), notice: 'Profilo aggiornato'
      else
        render :edit
      end
  end

  def destroy
    @user = User.find(params[:id]).destroy
    redirect_to root_path, notice: 'Utente eliminato'
    return
  end



  private
    # Utilizza i callback per condividere l'impostazione o i vincoli comuni tra le azioni.
    def set_user
      @user = User.find(params[:id])
    end

    # Consenti solo un elenco di parametri attendibili.
    def user_params
      params.require(:user).permit(:name, :surname, :email, :city, :province, :street, :birth, :desease, :password, :picture)
    end


    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless @user == current_user
    end

    # Conferma un utente amministratore
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end


end



