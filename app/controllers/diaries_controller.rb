class DiariesController < ApplicationController


    def index
    end

    def new
    end

    def show
        @diary = User.find(params[:user_id]).diary
        @user = User.find(params[:user_id])
    end


    def create
        @user = current_user
        @diary = current_user.build_diary(params.require(:diary).permit(:content))
        if @diary.save
            flash[:success] = "Diario creato"
            redirect_to user_diary_path(@user,@user.diary)
        else
            flash[:danger] = "ATTENZIONE! Non è stato possibile creare un diario vuoto"
            redirect_to user_path(current_user)
            return
        end
    end

    def edit
        @diary = User.find(params[:user_id]).diary
        @user = User.find(params[:user_id])

    end

    def update
        if !current_user.diary.blank?
            if @diary = current_user.diary.update(params.require(:diary).permit(:content))
               redirect_to user_diary_path(current_user,current_user.diary), notice: 'Diario aggiornato'
            else
               render :edit
            end
        else
            redirect_to user_path(current_user)
        end
    end

    def destroy
       if !current_user.diary.blank?
           current_user.diary.destroy
           flash[:success] = "Diario eliminato"
           redirect_to user_diary_path(current_user,current_user.diary)
       else
           redirect_to user_diary_path(current_user,current_user.diary), notice: 'Non esiste nessun diario ancora!'
       end

    end
end
