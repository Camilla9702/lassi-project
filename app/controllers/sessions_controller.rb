class SessionsController < ApplicationController
    def new
    end

    def create
        user = User.find_by(email: params[:session][:email].downcase)
        if user && user.authenticate(params[:session][:password])
            log_in user
            redirect_to user
            #if user.activated?
             #   log_in user
                # se viene spuntata la casella di ricordami
              #  params[:session][:remember_me] == '1' ? remember(user) : forget(user)
               # redirect_back_or user
            #else
             #   message  = "Account non attivato. "
              #  message += "Controlla la tua email per il link di attivazione."
               # flash[:warning] = message
                #redirect_to root_url
            #end
        else
            flash.now[:danger] = 'Email o password errati'
            render 'new'
        end
    end

    def destroy
        log_out if logged_in? # fa il logout solo se l'utente è ancora dentro
        redirect_to root_url
    end
end
