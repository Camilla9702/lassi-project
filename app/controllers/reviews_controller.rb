class ReviewsController < ApplicationController

  def index
  end

  def new
    @review = Review.new
    #@user = User.find(params[:user_id])
  end

  def create
    @user = User.find(params[:user_id])

    @review = @user.reviews.build(params.require(:review).permit(:body, :score, :user))
    if @review.save
        flash[:success] = "Recensione pubblicata"
        redirect_to user_diary_path(@user,@user.diary)
    else
        flash[:danger] = "Recensione non pubblicata"
        redirect_to root_path
    end
  end


  def edit
    @review = User.find(params[:user_id]).reviews.find(params[:id])
  end

  def show
    @review = User.find(params[:user_id]).reviews.find(params[:id]).paginate(page: params[:page])
    @user = User.find(params[:user_id])
  end

  def update
    @review = User.find(params[:user_id]).reviews.find(params[:id])

    if @review.update(review_params)
      redirect_to @review.user
    else
      render :edit
    end

  end


  def destroy
    #@user = User.find(params[:user_id])
    @review = current_user.reviews.find(params[:id])
    @review.destroy
    redirect_to user_diary_path(current_user,current_user.diary)

  end


   private

    def review_params
      p = params.require(:review).permit(:body, :score, :user)
      #{:body=> p[:body], :score=>p[:score], :user=>User.find(p[:user])}
    end

end
