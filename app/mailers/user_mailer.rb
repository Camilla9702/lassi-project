class UserMailer < ApplicationMailer

  default from: "noreply@example.com"

  # Attivazione dell'account
  def account_activation(user)
    @user = user
    mail to: user.email, subject: "Attivazione account"
  end


  # Reset della password
  def password_reset(user)
    @user = user
    mail to: user.email, subject: "Reset password"
  end
end
