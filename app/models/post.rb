class Post < ApplicationRecord
  belongs_to :user
  default_scope -> { order(created_at: :desc) } # post visualizzati in ordine decrescente,
                                                # dal più recente al più vecchio
  mount_uploader :picture, PictureUploader

  validates :user_id, presence: true

  validates :content, presence: true, length: {maximum: 140}

  validate :picture_size  # non validate(s) perchè è una validation personalizzata

  private
    # Convalida la dimensione di un'immagine caricata.
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "Dovrebbe essere inferiore a 5 MB")
      end
    end


end
