class User < ActiveRecord::Base

    has_many :posts, dependent: :destroy

    has_one :diary, dependent: :destroy

    has_many :reviews, dependent: :destroy

    devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :omniauthable, :omniauth_providers => [:facebook]

    mount_uploader :picture, PictureUploader

    validate :picture_size

    validates :name, presence: true, length: { maximum: 50 }
    validates :surname, presence: true, length: { maximum: 50 }
    validates :city, presence: true, length: { maximum: 50 }
    validates :desease, presence: true, length: {maximum: 50}
    validates :province, length: { maximum: 2 }
    #validates :birth, allow_blank: true, format: { with: /\A(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2}) \z/ }
    #validates :birth, format: { with: /\A(?:(?:(?:0[13578]|1[02])(\/)31)\1|(?:(?:0[1,3-9]|1[0-2])(\/)(?:29|30)\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:02(\/)29\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:(?:0?[1-9])|(?:1[0-2]))(\/)(?:0[1-9]|1\d|2[0-8])\4(?:(?:1[6-9]|[2-9]\d)?\d{2})\z/ }
    #validates :birty, format: { with:  /\A(3[01]|[12][0-9]|0?[1-9])/(1[0-2]|0?[1-9])/(?:[0-9] {2})?[0-9]{2}\z/ }


    def feed
        Post.where("user_id = ?", id)
    end

    def feed_admin
        post_ids =  "SELECT id FROM users"
        Post.where("user_id IN (#{post_ids})")
    end

    def feed_users
        post_ids =  "SELECT id FROM users"
        Post.where("user_id IN (#{post_ids})")
    end

    def self.search(search)
        key = "%#{search}%"
        if search
            where('name LIKE ? OR desease LIKE ?', key, key)
        else
            all
        end
    end

    def averageScore
        total = 0
        self.reviews.each do |r|
            total += r.score
        end

        return (total.to_f/self.reviews.count.to_f).round(2)
    end



    def User.from_omniauth(auth)
        where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
          user.email = auth.info.email
          user.password = Devise.friendly_token[0,20]
          user.name = auth.info.first_name
          user.surname = auth.info.last_name
          user.data = auth.info.birthday
          user.city = auth.info.city
          user.desease = auth.info.desease
          user.save

        end
    end

    def User.new_with_session(params, session)
        super.tap do |user|
          if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
            user.email = data["email"] if user.email.blank?
          end
        end
    end



    private

         # Convalida la dimensione di un'immagine caricata.
        def picture_size
            if picture.size > 5.megabytes
                errors.add(:picture, "Dovrebbe essere inferiore a 5 MB")
            end
        end
end
