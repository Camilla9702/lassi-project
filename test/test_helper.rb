ENV['RAILS_ENV'] ||= 'test'

require 'rails/test_help'
require_relative "../config/environment"

class ActiveSupport::TestCase
  fixtures :all

  # Restituisce vero se un utente di prova ha effettuato l'accesso.
  def is_logged_in?
    !session[:user_id].nil?
    notice: 'Riuscito'
  end

  # Accesso utente di prova
  def log_in_as(user, options = {})
    password    = options[:password]    || 'password'
    remember_me = options[:remember_me] || '1'
    if integration_test?
      post login_path, session: { email:       user.email,
                                  password:    password,
                                  remember_me: remember_me }
    else
      session[:user_id] = user.id
    end
  end

  private

    # Restituisce true all'interno di un test di integrazione.
    def integration_test?
      defined?(post_via_redirect)
    end
end
