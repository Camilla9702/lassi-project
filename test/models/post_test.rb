require 'test_helper'

class PostTest < ActiveSupport::TestCase

  def setup
    @user = users(:marco)
    # Come nel caso di new, build restituisce un oggetto in memoria ma non modifica il database.
    @post = @user.posts.build(content: "Ciao! Questo è il primo post")
  end

  test "should be valid" do
    assert @post.valid?
  end

  test "user id should be present" do
    @post.user_id = nil
    assert_not @post.valid?
  end

  test "content should be present " do
    @post.content = "   "
    assert_not @post.valid?
  end

  test "content should be at most 140 characters" do
    @post.content = "a" * 141
    assert_not @post.valid?
  end

  # Si vuole che i post più recenti siano visualizzati per primi
  test "order should be most recent first" do
    assert_equal Post.first, posts(:most_recent)
  end
end
