require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest

    def setup
        @user = users(:marco)
    end

    test "unsuccessful edit" do
        log_in_as(@user)
        get edit_user_path(@user)
        assert_template 'users/edit'
        name = "User name"
        email = "ema@bar.com"
        patch user_path(@user), user: { name:  "",
                                        surname: "",
                                        email: "ema@invalid",
                                        city: "",
                                        province: "",
                                        street: "",
                                        birth: "",
                                        desease: "",
                                        password: "pas",
                                        password_confirmation: "wor" }
        assert_not flash.empty?
        assert_redirected_to @user
        @user.reload
        assert_equal @user.name,  name
        assert_equal @user.email, email

    end

    test "successful edit" do
        log_in_as(@user)
        get edit_user_path(@user)
    end

end
