require 'test_helper'

class PostsInterfaceTest < ActionDispatch::IntegrationTest

    test "micropost interface" do
        log_in_as(@user)
        get root_path
        assert_select 'div.pagination'
       # Invio non valido
        assert_no_difference 'Post.count' do
          post posts_path, post: { content: "" }
        end

        assert_select 'div#error_explanation'
        # Invio valido
        content = "This post really ties the room together"
        assert_difference 'Post.count', 1 do
          post posts_path, post: { content: content }
        end

        assert_redirected_to root_url
        follow_redirect!
        assert_match content, response.body
        # Elimina un post.
        assert_select 'a', text: 'delete'
        first_post = @user.posts.paginate(page: 1).first
        assert_difference 'Post.count', -1 do
          delete post_path(first_post)
        end

        # Visita un altro utente.
        get user_path(users(:alexander))
        assert_select 'a', text: 'delete', count: 0
      end
end
