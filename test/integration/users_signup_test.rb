require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest

    def setup
        ActionMailer::Base.deliveries.clear
    end


    test "invalid signup information" do
        get signup_path
        assert_no_difference 'User.count' do
          post users_path, user: { name:  "",
                                   surname: "",
                                   email: "user@invalid",
                                   city: "",
                                   province: "",
                                   street: "Street",
                                   birth: "Birth",
                                   desease: "",
                                   password: "pas",
                                   password_confirmation: "wo" }

        end
        assert_template 'users/new'
        assert_select 'div#error_explanation'
        assert_select 'div.field_with_errors'
    end

    test "valid signup information" do
        get signup_path
        assert_difference 'User.count', 1 do
          post_via_redirect users_path, user: { name:  "Name User",
                                                surname: "Surname User",
                                                email: "user@example.com",
                                                city: "Cityyyyyy",
                                                province: "Provinceeee",
                                                street: "Streettttt",
                                                birth: "Birthhhh",
                                                desease: "Deseaseee",
                                                password: "password",
                                                password_confirmation: "password" }
        end
        assert_template 'users/show'
        assert is_logged_in?
    end

    test "valid signup information with account activation" do
        get signup_path
        assert_difference 'User.count', 1 do
          post users_path, user: { name:  "Example User",
                                   name:  "Name User",
                                                    surname: "Surname User",
                                                    email: "user@example.com",
                                                    city: "Cityyyyyy",
                                                    province: "Provinceeee",
                                                    street: "Streettttt",
                                                    birth: "Birthhhh",
                                                    desease: "Deseaseee",
                                                    password: "password",
                                                    password_confirmation: "password" }
        end
        assert_equal 1, ActionMailer::Base.deliveries.size
        user = assigns(:user)
        assert_not user.activated?
        # Prova a fare l'accesso prima l'attivazione.
        log_in_as(user)
        assert_not is_logged_in?
        # Token di attivazione non valido
        get edit_account_activation_path("invalid token")
        assert_not is_logged_in?
        # Token valido ma email errata
        get edit_account_activation_path(user.activation_token, email: 'wrong')
        assert_not is_logged_in?
        # Token di attivazione valido
        get edit_account_activation_path(user.activation_token, email: user.email)
        assert user.reload.activated?
        follow_redirect!
        assert_template 'users/show'
        assert is_logged_in?
    end

end
