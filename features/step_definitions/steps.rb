
Given /^I am a registered user$/ do
  @user = FactoryBot.create(:user)
  visit "users/sign_in"
  fill_in "user_email", :with => @user.email
  fill_in "user_password", :with => @user.password
  click_button "Accedi"
end

Given /^I am logged in$/ do
  visit "users/sign_in"
  @user = FactoryBot.create(:user)
  fill_in "user_email", :with => @user.email
  fill_in "user_password", :with => @user.password
  click_button "Accedi"
end

Given /^I am logged user with diary in$/ do
  @user = FactoryBot.create(:user)
  @diary = FactoryBot.create(:diary,:user_id => @user.id)
  visit "users/sign_in"
  fill_in "user_email", :with => @user.email
  fill_in "user_password", :with => @user.password
  click_button "Accedi"
end

Given /^I am the admin user$/ do
  @user = FactoryBot.create(:user, :admin => 1)
  @user.admin = 1
  @utente = FactoryBot.create(:user, :admin => 0)
  visit "users/sign_in"
  fill_in "user_email", :with => @user.email
  fill_in "user_password", :with => @user.password
  click_button "Accedi"
end

When /^I click (.*)/ do |element|
   click_on(element)
end

When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end

When /^(?:|I )follow '([^"]*)'$/ do |link|
  first(:link, link).click
end

When /^(?:|I )fill in "([^"]*)" with "([^"]*)"$/ do |field, value|
  fill_in(field, :with => value)
end

When /^I attach an "([^\"]*)" image to "([^\"]*)"$/ do |size, field|
  attach_file(field, image_path(size))
end

Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end


Then /^(?:|I )should be on (.+)$/ do |page_name|

  current_path = URI.parse(current_url).path
  if current_path.respond_to? :should
    expect(current_path).to be == path_to(page_name)
  else
    assert_equal path_to(page_name), current_path
  end
end


Then /^(?:|I )should see '([^"]*)'$/ do |text|
  if page.respond_to? :should
    expect(page).to have_content(text)
  else
    assert page.has_content?(text)
  end
end


Then /^(?:|I )should see \/([^\/]*)\/$/ do |regexp|
  regexp = Regexp.new(regexp)

  if page.respond_to? :should
    expect(page).to have_xpath('//*', :text => regexp)
  else
    assert page.has_xpath?('//*', :text => regexp)
  end
end


Then /^(?:|I )should not see '([^"]*)'$/ do |text|
  if page.respond_to? :should
    expect(page).to have_no_content(text)
  else
    assert page.has_no_content?(text)
  end
end

When /^(?:|I )press '([^"]*)'$/ do |button|
  click_button(button)
end


And /^I have an account$/ do
  @user = FactoryBot.create(:user)
end


And /^I insert my credentials$/ do
    fill_in "user_email", :with => @user.email
    fill_in "user_password", :with => @user.password
end








