Feature: User can edit his profile
        As a User
        I want to edit my profile

	Scenario: Editing my account
	   Given I am a registered user
	   And I am on the homepage
	   When I click Profilo
	   And I click Modifica
	   Then I should be on the edit page
	   And I click Salva
