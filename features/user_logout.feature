Feature: User can logout

	Scenario: Logout success
	    Given I am on the homepage
	    And I am logged in
	    When I click Log out
	    Then I should be on the welcome page
	    And I should see 'Accedi'
	    And I should see 'Registrati'

