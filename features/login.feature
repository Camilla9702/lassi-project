Feature: Guest can login
    	As a guest
    	I want to login with email
    	So that I can join it

        Scenario: Login success
            Given I am a registered user
            And I insert my credentials
            And I click Log in
            Then I should see 'Logout'

        Scenario: Login failed
            Given I am on the login page
            When I fill in "user_email" with "exam@email.com"
            And I fill in "user_password" with "dayday"
            And I click Log in
            Then I should be on the login page
