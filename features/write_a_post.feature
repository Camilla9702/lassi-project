Feature: As a user 
         I want to write a post
        
	Scenario: Write a post
	  Given I am a registered user
	  And I am on the homepage
          When I fill in "write_post" with "scrivi qui i tuoi pensieri"
          And I click Pubblica
          Then I should be on my profile page
       
        Scenario: Write a blank post
	  Given I am a registered user
	  And I am on the homepage
          When I fill in "write_post" with ""
          And I click Pubblica
          Then I should be on the homepage

	Scenario: Write a post with an image
	  Given I am a registered user
	  And I am on the homepage
          When I fill in "write_post" with "scrivi qui i tuoi pensieri"
          And I attach an "image_post" image to "picture_post"
          Then I click Pubblica
          And I should be on my profile page

