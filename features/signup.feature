Feature: Guest can create a new account
	As a guest
	I want to sign up
	So I can be a user

	Scenario: Create a new user account
		Given I am on the sign up page
		When I fill in "user[name]" with "Name"
		And I fill in "user[surname]" with "Surname"	
		And I fill in "user[email]" with "user@email.com"
		And I fill in "user[city]" with "City"
		And I fill in "user[province]" with "PR"
		And I fill in "user[street]" with "Street"
		And I fill in "user[desease]" with "Desease"
		And I fill in "user[password]" with "password"
		And I click Crea il mio account
		Then I should not see 'Sign In'
		Then I should be on the homepage
		


	Scenario: Can't create an account (desease required)
		Given I am on the sign up page
		When I fill in "user[name]" with "Name"
		And I fill in "user[surname]" with "Surname"
		And I fill in "user[email]" with "user@email.com"
		And I fill in "user[city]" with "City"
		And I fill in "user[province]" with "PR"
		And I fill in "user[street]" with "Street"
		And I fill in "user[password]" with "password"
		And I click Crea il mio account
		#Then I should be on the sign up page
		And I should see 'Desease can't be blank'
		
	        



