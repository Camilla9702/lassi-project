Feature: Homepage of the application
	As a Logged user
	I want to see the homepage
  	In order to search user, write a post and see users posts

        Scenario: Search user
           Given I am a registered user
	   And I am on the homepage
   	   When I fill in "search" with "user"
    	   And I click Cerca
   	   Then I should see 'I tuoi risultati'


	Scenario: Write a post
	   Given I am a registered user
	   And I am on the homepage
           And I fill in "write_post" with "scrivi qui i tuoi pensieri"
           And I click Pubblica
           Then I should be on my profile page
