Feature: User can delete his account
        As a User
        I want to delete my account


	Scenario: Deleting my account
	   Given I am a registered user
	   And I am on the homepage
	   Then I click Profilo
	   Then I click Cancella account
	   Then I should be on the welcome page
	   And I should see 'Accedi'
          

