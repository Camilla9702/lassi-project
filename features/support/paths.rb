module NavigationHelpers
  # Maps a name to a path. Used by the
  #
  #   When /^I go to (.+)$/ do |page_name|
  #
  # step definition in steps.rb
  #

  def path_to(page_name)
    case page_name

    when /^the welcome page$/
      '/'

    when /^the homepage$/
      '/'

    when /^the sign up page$/
      '/users/sign_up'

    when /^my profile page$/
      user_path(@user.id)

    when /^the login page$/
      '/users/sign_in'

    when /^the edit page$/
      edit_user_path(@user.id)

    when /^the create diary page$/
      new_user_diary_path(@user,@user.diary)

    when /^the edit diary page$/
      edit_user_diary_path(@user,@user.diary)


    else
      begin
        page_name =~ /^the (.*) page$/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue NoMethodError, ArgumentError
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
end

World(NavigationHelpers)



module ImageHelpers
  def image_path(size)
    case size
    when "image_post"
      File.expand_path(File.join(File.dirname(__FILE__),"image.jpg"))
      # NOTE: I created a sub dir of "support" called "fixtures" in which to place test files.
    else
      raise "don't know where to find a #{size} image"
    end
  end
end

World(ImageHelpers)



