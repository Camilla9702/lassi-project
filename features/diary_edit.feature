Feature: As a user 
         I want to edit my diary
        
	Scenario: Edit my diary
	   Given I am logged user with diary in
           And I am on my profile page
           When I click Diario
	   And I click Modifica diario
           Then I should be on the edit diary page
           And I should see 'Modifica il tuo diario'
           
