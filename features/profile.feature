Feature: I want to see my profile page

	Scenario: As a user I want to see my profile page
	    Given I am on the home page
	    And I am logged in
	    When I click Profile
	    Then I should see "My profile"
	    Then I should see "My posts"

	Scenario: As a user I want to see my diary on my profile page
	    Given I am on the home page
	    And I am logged in
	    When I click Profile
	    Then I should see "My diary"
