Feature: Guest can login
    	As a guest
    	I want to login with email
    	So that I can join it

        Scenario: Login success
            Given I am a registered user
	    And I click Log out
	    Then I should be on the welcome page
            And I click Accedi
            And I insert my credentials
            Then I click Accedi
            And I should see 'Log out'
            And I should see 'Profilo'

        Scenario: Login failed (password too short)
            Given I am on the login page
            When I fill in "user_email" with "exam@email.com"
            And I fill in "user_password" with "day"
            And I click Accedi
            Then I should be on the login page
            And I should see 'Invalid Email or password'
