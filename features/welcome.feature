Feature: Welcome
	As a Guest
	I want to see the welcome page of my app
  	In order to login o sign up
  	

	Scenario: View welcome page
  		Given I am on the welcome page
  		Then I should see 'Benvenuto in CoViD'
                Then I should see 'Info'
                Then I should see 'About us'
		Then I should see 'Contact us'
        
        Scenario: Guest can login
		Given I am on the welcome page
		And I click Accedi
		Then I should be on the login page

        Scenario: Guest can sign up
		Given I am on the welcome page
		And I click Registrati
		Then I should be on the sign up page

	
	
	 
