FactoryBot.define do
require 'faker'

  factory :post do 
    user_id     { Faker::IDNumber.valid }
    content     { Faker::Lorem.sentence(word_count: 6) }
    picture     { Faker::LoremPixel.image(size: "50x60") }
  end
end
