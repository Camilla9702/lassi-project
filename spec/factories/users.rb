FactoryBot.define do
require 'faker'
  factory :user do
    name        {Faker::Name.first_name}
    surname     {Faker::Name.last_name}
    email       {Faker::Internet.safe_email}
    password    {"password"}
    birth       {Faker::Date.birthday(min_age:13,max_age:99)}
    city        {Faker::Address.city}
    desease     {"desease"}
  end

end

