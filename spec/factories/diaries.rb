FactoryBot.define do
require 'faker'
  factory :diary do 
    user_id     { Faker::IDNumber.valid }
    content     { "Ciao questo è il mio diario e oggi racconterò di me" }
  end
end
