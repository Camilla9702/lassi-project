require 'rails_helper'

require "./app/models/post.rb"

RSpec.describe PostsController, type: :controller do

    before(:each) do
        @user = FactoryBot.create(:user)
        @post = FactoryBot.create(:post, user_id: @user.id)
    end


    describe "POST #create" do
        context "with valid params" do
                it "creates a new Post" do
                    allow(controller).to receive(:authenticate_user!).and_return(true)
                    allow(controller).to receive(:current_user).and_return(@user)
                    expect{post :create, params: {
                        post: {
                           content: @post.content,
                           picture: @post.picture
                        }}}.to change(Post, :count).by(1)
                            expect(response).to redirect_to(user_path(@user))
                end
        end

         context "with blank content" do
                it "creates a flash notify error" do
                    allow(controller).to receive(:authenticate_user!).and_return(true)
                    allow(controller).to receive(:current_user).and_return(@user)
                    expect{post :create, params: {
                       post: {
                            content: nil,
                            picture: @post.picture
                        }}}.to change(Post, :count).by(0)
                            expect(response).to redirect_to(root_path)
                end
          end
    end


    describe "DELETE #destroy" do
          context "with valid user id" do
             it "destroy an existent post" do
                allow(controller).to receive(:authenticate_user!).and_return(true)
                allow(controller).to receive(:current_user).and_return(@user)
                expect{delete :destroy, params: {id: @post.user_id 
                      }}.to change(Post, :count).by(-1) 
                         expect(response).to redirect_to(root_path)
             end
           end


          context "with invalid user id" do
            it "redirect to 404" do
                allow(controller).to receive(:authenticate_user!).and_return(true)
		allow(controller).to receive(:current_user).and_return(@user)
                expect{delete :destroy, params: { id: 9999
                      }}.to change(Post, :count).by(0)
			 expect(response).to have_http_status(302)
            end
          end
    end
end









