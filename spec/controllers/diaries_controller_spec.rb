require 'rails_helper'

RSpec.describe DiariesController, type: :controller do

    before(:each) do
        @user = FactoryBot.create(:user)
        @diary = FactoryBot.create(:diary, user_id: @user.id)
    end


    describe "POST #create" do

         context "with valid content" do
                it "creates a new Diary" do
                    allow(controller).to receive(:authenticate_user!).and_return(true)
                    allow(controller).to receive(:current_user).and_return(@user)
                    expect{post :create, params: {
                        diary: { content: @diary.content}
                    }}.to expect(response).to redirect_to(user_diary_path(@user,@user.diary))
                end
        end


        context "with blank content" do
            it "creates a flash notify error" do
                    allow(controller).to receive(:current_user).and_return(@user)
                    expect{post :create, params: {content: nil}}
                    expect(response).to redirect_to(user_path(@user))
=begin
                    allow(controller).to receive(:authenticate_user!).and_return(true)
                    allow(controller).to receive(:current_user).and_return(@user)
                    expect{post :create, params: {
                       diary: {
                            content: nil
                        }}}.to expect(response).to redirect_to(user_path(@user.id))
                    #expect{post :create,
                     #      :params => { :diary => {:content => nil}
                      #  }}.to expect(response).to redirect_to user_path(@user)
=end
            end
        end
    end
end
