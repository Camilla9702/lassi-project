class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.string :surname
      t.string :email
      t.string :city
      t.string :province
      t.string :street
      t.date :birth
      t.string :desease
      t.string :password

      t.timestamps
    end
  end
end
