class AddUserReviewedIdToReviews < ActiveRecord::Migration[5.2]
  def change
    add_column :reviews, :user_reviewed_id, :integer
  end
end
