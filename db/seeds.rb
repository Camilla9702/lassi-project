# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
require 'faker'
I18n.reload!


admin = User.create!(name:  "Camilla",
             surname: "Cariello",
             email: "milly972003@live.it",
             password:              "Camilla97",
             password_confirmation: "Camilla97",
             city: "Città",
             desease: "Malattia",
             admin:     true,
             activated: true,
             activated_at: Time.zone.now)

admin.save!

User.create!(name:  "Marco",
             surname: "Rossi",
             email: "marcorossi@gmail.com",
             password:              "Marcorossi",
             password_confirmation: "Marcorossi",
             city: "Roma",
             desease: "Malattia",
             admin:     false,
             activated: true,
             activated_at: Time.zone.now)

User.create!(name:  "Isabella",
             surname: "Castani",
             email: "isacastani@gmail.com",
             password:              "Isacastani",
             password_confirmation: "Isacastani",
             city: "Perugia",
             desease: "Malattia",
             admin:     false,
             activated: true,
             activated_at: Time.zone.now)

User.create!(name:  "Lucio",
             surname: "Sestio",
             email: "luciosestio@gmail.com",
             password:              "Luciosestio",
             password_confirmation: "Luciosestio",
             city: "Roma",
             desease: "Malattia",
             admin:     false,
             activated: true,
             activated_at: Time.zone.now)

User.create!(name:  "Manuela",
             surname: "D'aragosta",
             email: "emaragosta@gmail.com",
             password:              "Emarag",
             password_confirmation: "Emarag",
             city: "L'Aquila",
             desease: "Malattia",
             admin:     false,
             activated: true,
             activated_at: Time.zone.now)

User.create!(name:  "Fabiana",
             surname: "Valentini",
             email: "fabianavale@gmail.com",
             password:              "Fabianavale",
             password_confirmation: "Fabianavale",
             city: "Ancona",
             desease: "Malattia",
             admin:     false,
             activated: true,
             activated_at: Time.zone.now)

User.create!(name:  "Biagio",
             surname: "Ponte",
             email: "biagioponte@gmail.com",
             password:              "Biagioponte",
             password_confirmation: "Biagioponte",
             city: "L'Aquila",
             desease: "Malattia",
             admin:     false,
             activated: true,
             activated_at: Time.zone.now)


10.times do |n|
  name  = Faker::Name.first_name
  surname = Faker::Name.last_name
  email = Faker::Internet.email
  password = "password"
  city = Faker::Address.city
  desease = "desease"


  User.create!(name:  name,
              surname: surname,
              email: email,
              password:              password,
              password_confirmation: password,
              city: city,
              desease: desease,
              activated: true,
	      admin:     false,
              activated_at: Time.zone.now)
end


users = User.order(:created_at).take(6)
10.times do
  content = Faker::Lorem.sentence
  users.each { |user| user.posts.create!(content: content) }
end
